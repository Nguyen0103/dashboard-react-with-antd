import './App.css';

import Header from "./components/Header";
import PageContent from "./components/PageContent";
import SideBar from "./components/SideBar";
import Footer from "./components/Footer";
import { Space } from 'antd';
import MultiMenu from './components/MultiMenu/MultiMenu';
import { menuData } from './components/MultiMenu/MenuData';



function App() {
  return (
    <div className="App">
      <Header />

      <Space className='sideBarAndPageContent'>
        <SideBar></SideBar>
        {/* <MultiMenu menuData={menuData} /> */}
        <PageContent></PageContent>
      </Space>

      <Footer />
    </div>
  );
}

export default App;
