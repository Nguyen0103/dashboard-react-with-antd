import React, { useEffect, useState } from 'react'
import { Card } from 'antd';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { apiServices } from '../../../API/api';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

function StatisticBoard() {
    const [revenueData, setRevenheData] = useState({ labels: [], datasets: [] })
    const [inventoryData, setInventoryData] = useState({ labels: [], datasets: [] })

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'bottom',
            },
        },
    };

    useEffect(() => {
        apiServices.getRevenue()
            .then((res) => {
                const labels = res.carts.map((cart) => `User-${cart.userId}`)
                const data = res.carts.map((cart) => cart.discountedTotal)
                const dataSource = {
                    labels,
                    datasets: [
                        {
                            label: 'Revenue',
                            data: data,
                            backgroundColor: 'red',
                        },
                    ],
                };
                setRevenheData(dataSource)
            })
            .catch((err) => {
                throw err;
            });

        apiServices.getInventory()
            .then((res) => {
                const labels = res.products.map(product => product.title)
                const data = res.products.map((product) => product.stock)
                const dataSource = {
                    labels,
                    datasets: [
                        {
                            label: 'All products',
                            data: data,
                            backgroundColor: 'Blue',
                        },
                    ],
                };
                setInventoryData(dataSource)
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <Card style={{ width: 500, heigh: 350 }}>
            <Bar options={options} data={revenueData} />
            <Bar options={options} data={inventoryData} />
        </Card>
    )
}

export default StatisticBoard