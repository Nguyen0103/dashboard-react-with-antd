import { Table, Typography } from 'antd'
import React, { useEffect, useState } from 'react'
import { apiServices } from '../../../API/api';

function RecentOrders() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true)
        apiServices.getOrder()
            .then((res) => {
                setDataSource(res.products.splice(0, 5));
                setLoading(false);
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
            });
    }, [])

    return (
        <>
            <Typography.Text>Recent Orders</Typography.Text>
            <Table
                loading={loading}
                pagination={false}
                columns={[
                    {
                        title: 'Title',
                        dataIndex: 'title'
                    },
                    {
                        title: 'Quantity',
                        dataIndex: 'quantity'
                    },
                    {
                        title: 'Price',
                        dataIndex: 'price',
                    },
                ]} dataSource={dataSource} />

        </>
    )
}

export default RecentOrders