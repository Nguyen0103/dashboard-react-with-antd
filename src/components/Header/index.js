import { Badge, Button, Drawer, Image, List, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { Typography } from 'antd';
import { BellFilled, MailOutlined } from '@ant-design/icons'
import style from "./Header.module.scss";
import { apiServices } from '../../API/api';

const { Title } = Typography;


function Header() {

    const [comments, setComments] = useState([])
    const [orders, setOrders] = useState([])

    const [commentsOpen, setCommentsOpen] = useState(false);
    const [notificationsOpen, setNotificationsOpen] = useState(false);

    useEffect(() => {
        apiServices.getComments()
            .then((res) => {
                setComments(res.comments);
            })
            .catch((err) => {
                console.log(err);
            });

        apiServices.getOrder()
            .then((res) => {
                setOrders(res.products)
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <div className={style.header}>
            <div className={style.container}>
                <Image className={style.logo} width={60} src="https://img.freepik.com/free-icon/pie-chart_318-186793.jpg?size=338&ext=jpg"></Image>

                <Title>DashBoard</Title>

                <Space size={"large"}>

                    {/* Comments */}
                    <Badge count={comments.length} dot>
                        <MailOutlined style={{ fontSize: "30px", }} onClick={() => setCommentsOpen(true)} />
                    </Badge>

                    {/* Post */}
                    <Badge count={orders.length}>
                        <BellFilled style={{ fontSize: "30px", }} onClick={() => setNotificationsOpen(true)} />
                    </Badge>

                    <Drawer
                        title={<Typography.Title>comments</Typography.Title>}
                        open={commentsOpen}
                        onClose={() => setCommentsOpen(false)}
                        closable={false}
                        placement='right'
                        width={700}>
                        <List dataSource={comments} renderItem={({ body, user }) => {
                            return (<List.Item>
                                <Space direction='horizontal'>
                                    <Typography.Title level={3} strong >
                                        {user.username}
                                    </Typography.Title>
                                    :{body}
                                </Space>
                            </List.Item>)
                        }} />
                    </Drawer>

                    <Drawer
                        title={<Typography.Title>Notifications</Typography.Title>}
                        open={notificationsOpen}
                        onClose={() => setNotificationsOpen(false)}
                        closable={false}
                        placement='right'
                        width={700}>
                        <List dataSource={orders} renderItem={({ title }) => {
                            return (<List.Item>
                                <Space direction='horizontal'>
                                    <Typography.Text strong>{title}:</Typography.Text> has been Ordered!
                                </Space>
                            </List.Item>)
                        }} />
                    </Drawer>
                </Space>
            </div >

        </div >
    )
}

export default Header