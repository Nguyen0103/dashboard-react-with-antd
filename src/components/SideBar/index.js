import {
    AppstoreOutlined,
    ShopOutlined,
    ShoppingCartOutlined,
    UserOutlined,
} from '@ant-design/icons'
import { Menu } from 'antd'
import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from "react-router-dom";
import style from './SideBar.module.scss'


function SideBar() {
    const location = useLocation();
    const navigate = useNavigate();

    const [selectedKeys, setSelectedKeys] = useState("/")

    useEffect(() => {
        const pathName = location.pathname
        console.log(pathName)
        setSelectedKeys(pathName)
    }, [location.pathname])

    const handleNavigate = (item) => {
        navigate(item.key)
    }

    return (
        <div className={style.sideBar}>
            <Menu
                mode='vertical'
                onClick={handleNavigate}
                selectedKeys={selectedKeys}
                items={[
                    { label: "DashBoard", key: "/", icon: <AppstoreOutlined /> },
                    { label: "Inventory", key: "/inventory", icon: <ShoppingCartOutlined /> },
                    { label: "Orders", key: "/orders", icon: <ShopOutlined /> },
                    { label: "Customers", key: "/customers", icon: <UserOutlined /> },
                ]}>
            </Menu>
        </div>
    )
}

export default SideBar