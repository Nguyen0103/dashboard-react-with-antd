import { Typography } from 'antd'
import React from 'react'
import style from './Footer.module.scss';

const { Link } = Typography
function Footer() {
    return (
        <div className={style.footer}>
            <Link target={"_blank"} href='tel:+84363788658'>+84363788658</Link>
            <Link target={"_blank"} href='https://www.google.com'>Privacy Policy</Link>
            <Link target={"_blank"} href='https://facebook.com/sliver.key.14'>Terms of use</Link>
        </div>
    )
}

export default Footer