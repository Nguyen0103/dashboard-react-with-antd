import React from 'react'
import RoutesApp from '../../Routes'
import style from './PageContent.module.scss'

function PageContent() {
    return (
        <div className={style.pageContent}>
            <RoutesApp />
        </div>
    )
}

export default PageContent