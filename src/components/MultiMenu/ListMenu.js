import React, { useEffect, useState } from 'react'
import SubMenu from './SubMenu';
import "./MultiMenu.scss";

function ListMenu({ dept, data, menuIndex, hasSubMenu, menuName }) {
    const [activeMenu, setActiveMenu] = useState([])

    const handleArrowClick = (menuName) => {
        let cloneActiveMenu = [...activeMenu];

        if (cloneActiveMenu.includes(menuName)) {
            let index = activeMenu.indexOf(menuName)
            if (index > -1) {
                cloneActiveMenu.splice(index, 1);
            }
        } else {
            cloneActiveMenu.push(menuName)
        }

        setActiveMenu(cloneActiveMenu)
    }

    return (
        <li>
            <div dept={dept} style={{ display: "flex", padding: "12px 18px", paddingLeft: `${dept * 18}px`, alignItems: 'center' }}>
                <label className='title'>{data.label}</label>
                {
                    hasSubMenu && <span className='arrow' onClick={() => handleArrowClick(menuName)}></span>
                }
            </div>
            <div>
                {
                    hasSubMenu && <SubMenu dept={dept} data={data.submenu} menuIndex={menuIndex} toggle={activeMenu.includes(menuName)} />
                }
            </div>
        </li >
    )
}

export default ListMenu