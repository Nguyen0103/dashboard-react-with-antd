import React from 'react'
import ListMenu from './ListMenu'
import "./MultiMenu.scss";

function SubMenu({ dept, data, menuIndex, toggle }) {
    if (!toggle) {
        return null;
    } else {
        dept = dept + 1;
    }

    return (
        <ul className='multi-ul'>
            {
                data.map((item, index) => {
                    const menuName = `menu-${dept}-${menuIndex}-${index}`

                    return (
                        <ListMenu
                            dept={dept}
                            data={item}
                            hasSubMenu={item.submenu}
                            menuName={menuName}
                            key={menuName}
                            menuIndex={index}
                        />
                    )
                })
            }
        </ul>
    )
}

export default SubMenu;