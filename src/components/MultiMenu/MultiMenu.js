import React, { useEffect } from 'react'
import ListMenu from './ListMenu'
import "./MultiMenu.scss";

function MultiMenu({ menuData }) {

    return (
        <ul className='multi-ul'>
            {
                menuData.map((menu, index) => {
                    let dept = 1;
                    const menuName = `menu-${dept}-${index}`

                    return (
                        <ListMenu
                            dept={dept}
                            data={menu}
                            menuIndex={index}
                            hasSubMenu={menu.submenu}
                            menuName={menuName}
                            key={menuName}
                        />
                    )
                })
            }
        </ul>
    )
}

export default MultiMenu