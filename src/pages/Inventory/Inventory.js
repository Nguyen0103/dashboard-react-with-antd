import { Avatar, Rate, Space, Table, Typography } from 'antd'
import React, { useEffect, useState } from 'react'
import { apiServices } from '../../API/api'

function Inventory() {

    const [loading, setLoading] = useState(false)

    const [dataSource, setDataSource] = useState([])

    useEffect(() => {
        setLoading(true)
        apiServices.getInventory()
            .then((res) => {
                setDataSource(res.products)
                setLoading(false)
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <Space direction='vertical' >
            <Typography.Title>Inventory</Typography.Title>
            <Table
                loading={loading}
                columns={[
                    {
                        title: <Typography.Text level={5}>Image</Typography.Text>,
                        dataIndex: 'thumbnail',
                        render: (image) => <Avatar src={image} />
                    },
                    {
                        title: <Typography.Text level={5}>Product Name</Typography.Text>,
                        dataIndex: 'title'
                    },
                    {
                        title: <Typography.Text level={5}>Brand</Typography.Text>,
                        dataIndex: 'brand'
                    },

                    {
                        title: <Typography.Text level={5}>Rating</Typography.Text>,
                        dataIndex: 'rating',
                        render: (rating) => <Rate value={rating} allowHalf={true} disabled />
                    },
                    {
                        title: <Typography.Text level={5}>Stock</Typography.Text>,
                        dataIndex: 'stock'
                    },
                    {
                        title: <Typography.Text level={5}>Category</Typography.Text>,
                        dataIndex: 'category'
                    },
                    {
                        title: <Typography.Text level={5}>Price</Typography.Text>,
                        dataIndex: 'price',
                        render: (price) => <span>${price}</span>
                    }
                ]} dataSource={dataSource} pagination={{
                    position: ['bottomRight'],
                    pageSize: 5,
                    defaultPageSize: 1,
                }} />
        </Space>
    )
}

export default Inventory