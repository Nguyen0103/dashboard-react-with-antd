import { Space, Table, Typography } from 'antd'
import React, { useEffect, useState } from 'react'
import { apiServices } from '../../API/api'

function Orders() {
    const [dataSource, setDataSource] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        apiServices.getOrder()
            .then((res) => {
                setDataSource(res.products)
                setLoading(false)
            })
            .catch((err) => {
                console.log(err);
                setLoading(false)
            });
    }, [])

    return (
        <Space direction='vertical'>
            <Typography.Title>Order</Typography.Title>
            <Table
                loading={loading}
                columns={[
                    {
                        title: <Typography.Text>Item Name</Typography.Text>,
                        dataIndex: 'title'
                    },
                    {
                        title: <Typography.Text>Price</Typography.Text>,
                        dataIndex: 'price',
                        render: (price) => <span>${price}</span>
                    },
                    {
                        title: <Typography.Text>Quantity</Typography.Text>,
                        dataIndex: 'quantity'
                    },
                    {
                        title: <Typography.Text>Total</Typography.Text>,
                        dataIndex: 'total',
                        render: (price) => <span>${price}</span>
                    },
                    {
                        title: <Typography.Text>DisCount Percent</Typography.Text>,
                        dataIndex: 'discountPercentage',
                        render: (percent) => <span>{percent}{" "}%</span>
                    },
                    {
                        title: <Typography.Text>Discounted Price</Typography.Text>,
                        dataIndex: 'discountedPrice',
                        render: (price) => <span>${price}</span>
                    },

                ]}
                dataSource={dataSource} pagination={{
                    position: ['bottomRight'],
                    pageSize: 5,
                    defaultPageSize: 1,
                }} />
        </Space>
    )
}

export default Orders