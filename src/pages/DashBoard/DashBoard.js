import React, { useEffect, useState } from 'react';
import { ShoppingOutlined } from '@ant-design/icons';
import { DollarCircleOutlined, ShoppingCartOutlined, UserOutlined } from '@ant-design/icons/lib/icons';
import { Space, Typography } from 'antd';
import { apiServices } from '../../API/api';
import DashboardCard from '../../layouts/components/DashboardCard/DashboardCard';
import RecentOrders from '../../layouts/components/RecentOrders/RecentOrders';
import StatisticBoard from '../../layouts/components/StatisticBoard/StatisticBoard';

function DashBoard() {

    const [order, setOrders] = useState(0);
    const [inventory, setInventory] = useState(0);
    const [customers, setCustomers] = useState(0);
    const [revenue, setRevenue] = useState(0);

    useEffect(() => {
        apiServices.getOrder()
            .then((res) => {
                setOrders(res.total)
                setRevenue(res.discountedTotal)
            })

        apiServices.getInventory()
            .then((res) => {
                setInventory(res.total)
            })

        apiServices.getCustomers()
            .then((res) => {
                setCustomers(res.total)
            })




    }, [])

    return (
        <Space size={20} direction='vertical' >
            <Typography.Title level={1}>DashBoard</Typography.Title>
            <Space direction='horizontal'>
                <DashboardCard
                    title={<Typography.Title level={4}>Orders</Typography.Title>}
                    value={order}
                    icon={<ShoppingCartOutlined style={{
                        color: "green",
                        backgroundColor: "rgba(0,255,0,0.5)",
                        padding: 8,
                        fontSize: 24,
                        borderRadius: "50%"
                    }} />} />

                <DashboardCard
                    title={<Typography.Title level={4}>Inventory</Typography.Title>}
                    value={inventory}
                    icon={<ShoppingOutlined style={{
                        color: "purple",
                        backgroundColor: "rgba(143,67,238,0.5)",
                        padding: 8,
                        fontSize: 24,
                        borderRadius: "50%"
                    }} />} />

                <DashboardCard
                    title={<Typography.Title level={4}>Customers</Typography.Title>}
                    value={customers}
                    icon={<UserOutlined style={{
                        color: "red",
                        backgroundColor: "rgba(235, 69, 95,0.5)",
                        padding: 8,
                        fontSize: 24,
                        borderRadius: "50%"
                    }} />} />

                <DashboardCard
                    title={<Typography.Title level={4}>Revenue</Typography.Title>}
                    value={revenue}
                    icon={<DollarCircleOutlined style={{
                        color: "yellowgreen",
                        backgroundColor: "rgba(255, 221, 131,0.5)",
                        padding: 8,
                        fontSize: 24,
                        borderRadius: "50%"
                    }} />} />
            </Space>

            <Space align='start'>
                <RecentOrders />
                <StatisticBoard />
            </Space>

        </Space>
    )
}

export default DashBoard;