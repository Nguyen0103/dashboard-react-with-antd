import { Avatar, Space, Table, Typography } from 'antd'
import Link from 'antd/es/typography/Link'
import React, { useEffect, useState } from 'react'
import { apiServices } from '../../API/api'

function Customers() {
    const [dataSource, setDataSource] = useState([])


    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        apiServices.getCustomers()
            .then((res) => {
                setDataSource(res.users)
                setLoading(false)
            })
            .catch((err) => {
                console.log(err);
                setLoading(false)
            });
    }, [])

    return (
        <Space direction='vertical'>
            <Typography.Title>Customers</Typography.Title>
            <Table
                loading={loading}
                columns={[
                    {
                        title: <Typography.Text>Image</Typography.Text>,
                        dataIndex: 'image',
                        render: (image) => <Avatar src={image} />
                    },
                    {
                        title: <Typography.Text>Name</Typography.Text>,
                        dataIndex: 'firstName',
                    },
                    {
                        title: <Typography.Text>Phone Number</Typography.Text>,
                        dataIndex: 'phone',
                        render: (phone) => <Link href={`tel:${phone}`}>{phone}</Link>
                    },
                    {
                        title: <Typography.Text>Email</Typography.Text>,
                        dataIndex: 'email',
                        render: (mail) => <Link href={`mailto:${mail}`}>{mail}</Link>

                    },
                    {
                        title: <Typography.Text>Address</Typography.Text>,
                        dataIndex: 'address',
                        render: (address) => <Space direction='vertical'>{address.address} {address.city}</Space>
                    },
                    {
                        title: <Typography.Text>Company</Typography.Text>,
                        dataIndex: 'company',
                        render: ({ address }) => <Space direction='vertical'>{address.address} {address.city}</Space>
                    }
                ]}
                dataSource={dataSource} pagination={{
                    position: ['bottomRight'],
                    pageSize: 5,
                    defaultPageSize: 1,
                }} />
        </Space>
    )
}

export default Customers