import httpsRequest from "./httpsRequest"


export const apiServices = {
    getOrder: () => {
        return httpsRequest.get('/carts/1')
    },

    getRevenue: () => {
        return httpsRequest.get('/carts')
    },

    getInventory: () => {
        return httpsRequest.get('/products')
    },

    getCustomers: () => {
        return httpsRequest.get('/users')
    },

    getComments: () => {
        return httpsRequest.get('/comments')
    },
}