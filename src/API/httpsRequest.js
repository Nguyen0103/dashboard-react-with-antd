import axios from "axios";


const httpsRequest = axios.create({
    baseURL: 'https://dummyjson.com/',
})


httpsRequest.interceptors.request.use(function (config) {
    return config;
}, function (error) {

    return Promise.reject(error);
});


httpsRequest.interceptors.response.use(function (response) {
    return response.data;
}, function (error) {
    return Promise.reject(error);
});
export default httpsRequest;