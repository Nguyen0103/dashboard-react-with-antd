import React from 'react'
import { Routes, Route } from 'react-router-dom';
import DashBoard from '../pages/DashBoard/DashBoard';
import Orders from '../pages/Orders/Orders';
import Inventory from '../pages/Inventory/Inventory';
import Customers from '../pages/Customers/Customers';

function RoutesApp() {
    return (
        <div>
            <Routes>
                <Route path='/' element={<DashBoard />} />
                <Route path='/orders' element={<Orders />} />
                <Route path='/inventory' element={<Inventory />} />
                <Route path='/customers' element={<Customers />} />
            </Routes>
        </div>
    )
}

export default RoutesApp;